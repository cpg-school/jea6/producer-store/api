package jea.producerstore.model;

import jea.producerstore.model.domain.Product;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static utils.ProductUtils.createProduct;
import static utils.ProductUtils.stringOfLength;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.Validator;
import java.util.*;

public class ProductTest {

    private Validator validator;

    @Before
    public void init() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void classExists() {
        Product product = new Product();
    }

    @Test
    public void productCanBeCreated_WithParameters() {
        Product product = new Product("TestProduct", "Dit is een test product", 9.99, "http://images.com/testproject.jpg");
    }

    @Test
    public void validateProduct_AllFieldsCorrect_GivesNoErrors() {
        Product product = createProduct();

        List<ConstraintViolation<Product>> violations = validate(product);

        assertTrue(violations.isEmpty());
    }

    @Test
    public void validateProduct_NameIsEmptyString_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setName("");

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        String expectedError = "Name cannot be empty.";
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_NameIsNull_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setName(null);

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        String expectedError = "Name cannot be empty.";
        ;
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validate_NameIsTooLong_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setName(stringOfLength(201));

        String expectedError = "Name cannot be longer than 200 characters.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));

    }

    @Test
    public void validateProduct_DescriptionIsBlank_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setDescription("");

        String expectedError = "Description cannot be empty.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_DescriptionIsNull_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setDescription(null);

        String expectedError = "Description cannot be empty.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_DescriptionTooShort_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setDescription(stringOfLength(19));

        String expectedError = "Description can contain 20 to 5000 characters.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_DescriptionTooLong_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setDescription(stringOfLength(5001));

        String expectedError = "Description can contain 20 to 5000 characters.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_PriceTooLow_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setPrice(0);

        String expectedError = "Price must be 0.01 or higher.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_ImageUrlEmpty_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setImageUrl("");

        String expectedError = "ImageUrl cannot be empty.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_ImageUrlIsNull_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setImageUrl(null);

        String expectedError = "ImageUrl cannot be empty.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_ImageUrlIsNotUrl_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setImageUrl("not_an_url");

        String expectedError = "ImageUrl needs to be a valid url.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_ImageUrlIsNotValid_GivesErrorWithMessage() {
        Product product = createProduct();
        product.setImageUrl("http://website.com/not_an_image.java");

        String expectedError = "ImageUrl needs to be a .jpg, .png or .gif.";

        List<ConstraintViolation<Product>> violations = validate(product);

        assertFalse(violations.isEmpty());
        assertEquals(expectedError, getErrorMessage(violations, expectedError));
    }

    @Test
    public void validateProduct_ImageUrlIsHttps_ProductIsValid() {
        Product product = createProduct();
        product.setImageUrl("https://website.com/https_image.png");

        List<ConstraintViolation<Product>> violations = validate(product);

        assertTrue(violations.isEmpty());
    }

    //region helper methods

    private List<ConstraintViolation<Product>> validate(Product p) {
        return new ArrayList<>(validator.validate(p));
    }



    private static String getErrorMessage(List<ConstraintViolation<Product>> violations, String message) {
        Optional<String> errorMessage = violations.stream().filter(v -> v.getMessage().equals(message)).map(ConstraintViolation::getMessage).findFirst();

        assertTrue("Error message is present in list.", errorMessage.isPresent());

        return errorMessage.get();
    }

    //endregion
}
