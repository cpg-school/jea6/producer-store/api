package jea.producerstore.web.socket;

import jea.producerstore.service.productWatch.WatchProductService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.verification.VerificationMode;

import javax.websocket.Session;

import java.util.HashMap;

import static org.mockito.Mockito.*;

public class WatchProductSocketTest {

    private WatchProductService serviceMock = mock(WatchProductService.class);
    private Session sessionMock = mock(Session.class);
    private WatchProductSocket socket;

    @Before
    public void before() {
        reset(serviceMock, sessionMock);
        socket = new WatchProductSocket(serviceMock);
    }

    @Test
    public void onMessage_usernameNull_messageNotForwarded() {
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<String, String>() {{
            put("productId", "1");
        }});

        socket.onMessage("test", sessionMock);

        verify(serviceMock, never()).sendMessage(anyString(), anyString(), anyString());
    }

    @Test
    public void onMessage_productIdNull_messageNotForwarded() {
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<String, String>() {{
            put("username", "test");
        }});

        socket.onMessage("test", sessionMock);

        verify(serviceMock, never()).sendMessage(anyString(), anyString(), anyString());
    }

    @Test
    public void onMessage_usernameAndProductIdNull_messageNotForwarded() {
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<>());

        socket.onMessage("test", sessionMock);

        verify(serviceMock, never()).sendMessage(anyString(), anyString(), anyString());
    }

    @Test
    public void onMessage_usernameAndProductIdProvided_messageIsForwarded() {
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<String, String>() {{
            put("username", "test");
            put("productId", "test");
        }});

        socket.onMessage("test", sessionMock);

        verify(serviceMock, times(1)).sendMessage(anyString(), anyString(), anyString());
    }

    @Test
    public void onOpen_productIdIsNull_noWatcherIsAddedAndOthersAreNotInformed() {
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<>());

        socket.onOpen(sessionMock);

        verify(serviceMock, never()).addWatcher(anyString(), any(Session.class));
        verify(serviceMock, never()).informChange(anyString());
    }

    @Test
    public void onOpen_productIdProvided_watcherIsAddedAndOthersAreInformed() {
        String productId = "test";
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<String, String>() {{
            put("productId", productId);
        }});

        socket.onOpen(sessionMock);

        verify(serviceMock, once()).addWatcher(productId, sessionMock);
        verify(serviceMock, once()).informChange(productId);
    }

    @Test
    public void onClose_productIdIsNull_watcherIsNotRemovedAndOthersAreNotInformed() {
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<>());

        socket.onClose(sessionMock);

        verify(serviceMock, never()).removeWatcher(anyString(), any(Session.class));
        verify(serviceMock, never()).informChange(anyString());
    }

    @Test
    public void onClose_productIdProvided_watcherIsRemovedAndOthersAreInformed() {
        String productId = "test";
        when(sessionMock.getPathParameters()).thenReturn(new HashMap<String, String>() {{
            put("productId", productId);
        }});

        socket.onClose(sessionMock);

        verify(serviceMock, once()).removeWatcher(productId, sessionMock);
        verify(serviceMock, once()).informChange(productId);
    }

    private static VerificationMode once() {
        return times(1);
    }
}