package jea.producerstore.web.api;

import jea.producerstore.app.exception.UnauthorizedException;
import jea.producerstore.model.dto.api.Credential;
import jea.producerstore.model.dto.api.JwtToken;
import jea.producerstore.model.dto.api.OauthResponse;
import jea.producerstore.model.domain.User;
import jea.producerstore.service.authentication.AuthenticationService;
import jea.producerstore.service.authentication.JwtService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class AuthenticationResourceTest  {

    private static JwtService managerMock = mock(JwtService.class);
    private static AuthenticationService serviceMock = mock(AuthenticationService.class);

    private static JerseyTest jersey;
    //region setup region

    @BeforeClass
    public static void setup() throws Exception {
        jersey = new JerseyTest() {
            @Override
            protected Application configure() {
                return new ResourceConfig(AuthenticateResource.class)
                        .register(new AbstractBinder() {
                            @Override
                            protected void configure() {
                                bind(managerMock).to(JwtService.class);
                                bind(serviceMock).to(AuthenticationService.class);
                            }
                        });
            }
        };

        jersey.setUp();
    }

    @AfterClass
    public static void teardown() throws Exception {
        jersey.tearDown();
    }

    @Before
    public void beforeEach() {
        reset(managerMock, serviceMock);
    }

    @Test
    public void authenticate_passwordAndUsernameNull_returnsUnauthorized() throws UnauthorizedException {
        Credential credential = new Credential();

        when(serviceMock.authenticate(any(Credential.class))).thenThrow(new UnauthorizedException());

        Response response = jersey.target("/authenticate").request().post(Entity.entity(credential, MediaType.APPLICATION_JSON));

        assertEquals(401, response.getStatus());
    }

    @Test
    public void authenticate_correctCredentials_returnsOkWithToken() throws Exception {
        Credential credential = new Credential();
        credential.setPassword("correct_password");
        credential.setUsername("correct_username");

        User user = new User();
        user.setUsername(credential.getUsername());
        user.setPassword(credential.getPassword());
        user.setId(1);
        user.setRole("user");

        JwtToken token = new JwtToken();
        token.setValue("token_value");

        when(serviceMock.authenticate(any(Credential.class))).thenReturn(user);
        when(managerMock.createToken(user.getId(), credential.getUsername(), user.getRole())).thenReturn(token);

        Response response = jersey.target("/authenticate").request().post(Entity.entity(credential, MediaType.APPLICATION_JSON));

        assertEquals(200, response.getStatus());
        assertEquals("Request did not return JwtToken type", JwtToken.class, response.readEntity(JwtToken.class).getClass());
    }

    @Test
    public void verifyGoogleOauth_invalidToken_returns401() {
        OauthResponse oauthResponse = new OauthResponse();
        oauthResponse.setValid(false);

        when(serviceMock.verifyOauth(anyString(), any())).thenReturn(oauthResponse);

        Response response = jersey.target("/authenticate/oath/google").request().post(Entity.entity("invalid_token", MediaType.TEXT_PLAIN));

        assertEquals(401, response.getStatus());
    }
}