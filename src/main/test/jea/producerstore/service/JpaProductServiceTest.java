package jea.producerstore.service;

import jea.producerstore.model.domain.Product;
import jea.producerstore.service.product.JpaProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.ws.rs.NotFoundException;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JpaProductServiceTest {

    @InjectMocks
    private JpaProductService service;

    @Mock
    private EntityManager managerMock;

    @Test
    public void getById_ProductExists_ReturnsIt() {
        long id = 1;
        Product product = new Product();
        product.setId(id);

        when(managerMock.find(Product.class, id)).thenReturn(product);

        Product foundProduct = service.getById(id);

        assertEquals(product, foundProduct);
    }

    @Test(expected = NotFoundException.class)
    public void getById_ProductDoesNotExist_ThrowsNotFoundException() {
        when(managerMock.find(eq(Product.class), any(UUID.class))).thenReturn(null);

        service.getById(1);
    }
}