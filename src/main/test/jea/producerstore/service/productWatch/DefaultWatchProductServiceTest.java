package jea.producerstore.service.productWatch;

import com.google.gson.Gson;
import jea.producerstore.model.dto.websocket.WatchProductChatMessage;
import org.junit.Before;
import org.junit.Test;

import javax.websocket.EncodeException;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class DefaultWatchProductServiceTest {
    private DefaultWatchProductService service;
    private String productId = "test";
    private Session session;

    @Before
    public void before() {
        service = new DefaultWatchProductService();
        session = mock(Session.class);
    }

    @Test
    public void addWatcher_watchersIncludeRecentlyAddedWatcher() {
        service.addWatcher(productId, session);

        List<Session> sessions = service.getWatchersForSession(productId);

        assertTrue(sessions.contains(session));
    }

    @Test
    public void removeWatcher_watchersDoesNotIncludeIt() {
        service.addWatcher(productId, session);
        service.removeWatcher(productId, session);

        List<Session> sessions = service.getWatchersForSession(productId);

        assertFalse(sessions.contains(session));
    }

    @Test
    public void sendMessage_AllSessionsAreInformed() throws IOException, EncodeException {
        List<Session> sessions = Arrays.asList(mock(Session.class), mock(Session.class), mock(Session.class));
        for (Session s : sessions) {
            when(s.getBasicRemote()).thenReturn(mock(RemoteEndpoint.Basic.class));
            service.addWatcher(productId, s);
        }
        String message = "Hello!";
        Gson gson = new Gson();

        String username = "test";
        service.sendMessage(username, productId, message);

        for (Session s: sessions) {
            verify(s.getBasicRemote(), times(1))
                    .sendObject(gson.toJson(new WatchProductChatMessage(username, productId, message)));
        }
    }
}