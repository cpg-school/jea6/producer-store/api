package utils;

import jea.producerstore.model.domain.Product;

import java.util.Arrays;

public class ProductUtils {
    public static Product createProduct() {
        Product product = new Product(stringOfLength(10), stringOfLength(25), 9.99, "http://source.com/image.png");
        product.setId(1);

        return product;
    }

    public static String stringOfLength(int n) {
        char[] charArray = new char[n];
        Arrays.fill(charArray, 'a');
        return new String(charArray);
    }
}
