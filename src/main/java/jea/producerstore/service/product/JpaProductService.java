package jea.producerstore.service.product;

import jea.producerstore.app.exception.CreationException;
import jea.producerstore.app.exception.DeletionException;
import jea.producerstore.app.exception.UpdateException;
import jea.producerstore.model.domain.Product;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import javax.ws.rs.NotFoundException;
import java.util.List;

@ApplicationScoped
public class JpaProductService implements ProductService {

    @PersistenceContext(unitName = "PU")
    private EntityManager manager;

    @Resource
    private UserTransaction transaction;

    @Override
    public List<Product> getAll() {
        return manager.createQuery("SELECT m FROM Product m", Product.class).getResultList();
    }

    @Override
    public Product getById(long id) {
        Product product = manager.find(Product.class, id);

        if (product == null) {
            throw new NotFoundException();
        }

        return product;
    }

    @Override
    public long add(Product product) throws CreationException {
        try {
            transaction.begin();
            manager.persist(product);
            manager.flush();
            transaction.commit();

            return product.getId();

        } catch (Exception e) {
            throw new CreationException("Could not create product.\n".concat(e.getMessage()));
        }
    }

    @Override
    public void delete(long id) throws DeletionException {
        Product product = manager.find(Product.class, id);

        if (product == null) {
            throw new NotFoundException();
        }

        try {
            transaction.begin();
            manager.remove(manager.contains(product) ? product : manager.merge(product));
            transaction.commit();

        } catch (NotSupportedException | HeuristicRollbackException | HeuristicMixedException | RollbackException | SystemException e) {
            throw new DeletionException(e);
        }
    }

    @Override
    public void update(long id, Product newValues) throws UpdateException {
        Product original = manager.find(Product.class, id);

        if (original == null) {
            throw new NotFoundException();
        }

        try {
            transaction.begin();

            newValues.setId(id);
            manager.merge(newValues);

            transaction.commit();
        } catch (NotSupportedException | HeuristicRollbackException | HeuristicMixedException | RollbackException | SystemException e) {
            throw new UpdateException(e);
        }
    }
}
