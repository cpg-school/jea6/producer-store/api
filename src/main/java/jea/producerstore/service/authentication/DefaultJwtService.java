package jea.producerstore.service.authentication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import jea.producerstore.model.dto.api.JwtToken;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

@ApplicationScoped
public class DefaultJwtService implements JwtService {

    private static final int TOKEN_VALIDITY = 4;
    private static final String ISSUER = System.getProperty("PRODUCERSTORE_JWT_ISSUER");
    private static final String AUDIENCE = System.getProperty("PRODUCERSTORE_JWT_AUDIENCE");
    private static final String SECRET = System.getProperty("PRODUCERSTORE_JWT_SECRET");
    private static final Algorithm HMAC256 = Algorithm.HMAC256(SECRET);

    @Override
    public JwtToken createToken(long subjectId, String subject, String... roles)  {
        return createToken(Long.toString(subjectId), subject, ISSUER, AUDIENCE, roles);
    }

    @Override
    public JwtToken createTokenForOauth(String subjectId, String subject, String issuer, String... roles)  {
        return createToken(subjectId, subject, AUDIENCE, issuer, roles);
    }

    private JwtToken createToken(String subjectId, String subject, String audience, String issuer, String... roles) {
        String token = JWT.create()
                .withSubject(subject)
                .withClaim("id", subjectId)
                .withIssuer(issuer)
                .withAudience(audience)
                .withIssuedAt(new Date())
                .withExpiresAt(toDate(LocalDate.now().plus(Period.ofDays(1))))
                .withArrayClaim(System.getProperty("PRODUCERSTORE_JWT_ROLE_CLAIM"), roles)
                .sign(HMAC256);

        return new JwtToken(token);
    }

    private Date toDate(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
