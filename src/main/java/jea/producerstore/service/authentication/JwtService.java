package jea.producerstore.service.authentication;

import jea.producerstore.model.dto.api.JwtToken;

public interface JwtService {

    /**
     * Create a JWT token for a specified subject.
     * @param subjectId the user id of the subject.
     * @param subject the username of the subject.
     * @param roles a list of security roles the user can claim.
     * @return A valid JWT token.
     */
    JwtToken createToken(long subjectId, String subject, String... roles);

    /**
     * Create a JWT token for a user that logged in via an oAuth provider.
     * @param subjectId the user id of the subject.
     * @param subject the name of the subject.
     * @param issuer the oauth provider ex. oauth/v2/google
     * @param roles a list of security roles the user can claim.
     * @return a valid JWT token.
     */
    JwtToken createTokenForOauth(String subjectId, String subject, String issuer, String... roles);
}
