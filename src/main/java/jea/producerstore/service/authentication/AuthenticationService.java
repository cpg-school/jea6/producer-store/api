package jea.producerstore.service.authentication;

import jea.producerstore.app.exception.UnauthorizedException;
import jea.producerstore.model.dto.api.Credential;
import jea.producerstore.model.dto.api.OauthResponse;
import jea.producerstore.model.dto.api.Oauth;
import jea.producerstore.model.domain.User;

public interface AuthenticationService {
    /**
     * Authenticate a user with a username and password.
     * @param credential The credentials of the user.
     * @return The user associated with given username and password.
     * @throws UnauthorizedException The given username and password did not match any known user.
     */
    User authenticate(Credential credential) throws UnauthorizedException;

    /**
     * Verify a given oAuth access token for a specific vendor (Google, Facebook, etc)
     * @param accessToken The access token received from the provider.
     * @param vendor The service that provided the access token.
     * @return A response whether the request was valid.
     */
    OauthResponse verifyOauth(String accessToken, Oauth vendor);
}
