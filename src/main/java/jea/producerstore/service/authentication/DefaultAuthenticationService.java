package jea.producerstore.service.authentication;

import jea.producerstore.app.exception.UnauthorizedException;
import jea.producerstore.model.domain.User;
import jea.producerstore.model.dto.api.Credential;
import jea.producerstore.model.dto.api.Oauth;
import jea.producerstore.model.dto.api.OauthResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.StringReader;

import static jea.producerstore.app.util.JpaUtillities.getSingleResult;

@ApplicationScoped
public class DefaultAuthenticationService implements AuthenticationService {

    @Inject
    private OkHttpClient httpClient;

    @PersistenceContext(name = "PU")
    private EntityManager manager;

    @Override
    public User authenticate(Credential credential) throws UnauthorizedException {
        User user = getSingleResult(manager.createQuery("SELECT u FROM User u WHERE username = :username AND password = :password", User.class)
                .setParameter("password", credential.getPassword())
                .setParameter("username", credential.getUsername()));

        if (user == null) {
            throw new UnauthorizedException();
        }

        return user;
    }

    @Override
    public OauthResponse verifyOauth(String accessToken, Oauth vendor) {
        OauthResponse response = new OauthResponse();

        Request request = new Request.Builder().url("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accessToken).build();

        try (Response verifyResponse = httpClient.newCall(request).execute()) {
            JsonObject json = Json.createReader(new StringReader(verifyResponse.body().string())).readObject();

            response.setUsername(json.getString("name"));
            response.setUserId(json.getString("id"));
            response.setValid(true);

        } catch (Exception e) {
            response.setValid(false);
        }

        return response;
    }
}
