package jea.producerstore.app.interceptor;

import com.google.gson.Gson;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

public class LoggedInterceptor {

    private Gson json = new Gson();

    @AroundInvoke
    public Object invoke(InvocationContext context) throws Exception {
        Logger logger = Logger.getLogger(context.getMethod().getDeclaringClass().getName());
        logMethodCall(context, logger);
        return logResult(context, logger);
    }

    private Object logResult(InvocationContext context, Logger logger) throws Exception {
        try {
            Object result = context.proceed();
            logger.info("Method succeeded");
            return result;
        } catch (Exception e) {
            logger.info("Method failed with exception: " + "(" + e.getClass().getSimpleName() + ")" + e.getMessage());
            throw e;
        }
    }

    private void logMethodCall(InvocationContext context, Logger logger) {
        StringBuilder message = new StringBuilder("\"")
                .append(context.getMethod().getName())
                .append("\"")
                .append(" invoked with parameters: ");

        for (Object parameter : context.getParameters()) {
            message.append("\"")
                    .append(json.toJson(parameter)).append("\"")
                    .append(", ");
        }

        logger.info(message.toString());
    }
}
