package jea.producerstore.app.util;

import javax.persistence.TypedQuery;
import java.util.List;

public class JpaUtillities {

    /**
     * Get a single result from a JPA query.
     * @param query The query to be executed.
     * @param <T> The type of the expected result.
     * @return The first result matching the given query, if nothing is found null is returned.
     */
    public static <T> T getSingleResult(TypedQuery<T> query) {
        query.setMaxResults(1);

        List<T> list = query.getResultList();

        if (list == null || list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }
}
