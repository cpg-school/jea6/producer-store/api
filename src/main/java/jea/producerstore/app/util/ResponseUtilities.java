package jea.producerstore.app.util;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ResponseUtilities {

    /**
     * Create a basic 404 response with a simple error message.
     * @param id The ID of the resource.
     * @param classs The type of the resource.
     * @param <T> The type of the resource/
     * @return A basic 404 response with a simple error message.
     */
    public static <T> Response notFound(long id, Class<T> classs) {
        return Response.status(404)
                .entity(classs.getSimpleName()
                        .concat(" with id \"")
                        .concat(Long.toString(id))
                        .concat("\" does not exist.")
                )
                .type(MediaType.TEXT_PLAIN_TYPE)
                .build();
    }

    /**
     * Create a basic error (HTTP 500) response.
     * @param message The message to be displayed to the client.
     * @return A basic error response.
     */
    public static Response internalServerError(String message) {
        return Response.status(500)
                .entity(message)
                .type(MediaType.TEXT_PLAIN_TYPE)
                .build();
    }
}
