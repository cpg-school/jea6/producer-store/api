package jea.producerstore.app.exception;

public class UnauthorizedException extends Exception {

    public UnauthorizedException() {

    }

    public UnauthorizedException(String s) {
        super(s);
    }
}
