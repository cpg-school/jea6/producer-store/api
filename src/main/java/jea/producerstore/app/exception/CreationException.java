package jea.producerstore.app.exception;

public class CreationException extends Exception {
    public CreationException(String errorMessage) {
        super(errorMessage);
    }
}
