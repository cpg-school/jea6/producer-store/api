package jea.producerstore.app.producer;

import okhttp3.OkHttpClient;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class SimpleProducer {

    /**
     * Creates an simple OkHttpClient to be injected when necessary.
     * @return The OkHttpClient.
     */
    @Produces
    public OkHttpClient createOkHttpClient() {
        return new OkHttpClient();
    }
}
