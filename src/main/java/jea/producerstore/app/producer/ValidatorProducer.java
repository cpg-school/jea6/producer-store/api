package jea.producerstore.app.producer;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Produces;
import javax.validation.Validator;

@ApplicationScoped
public class ValidatorProducer {

    /**
     * Creates a Validator to be injected when necessary.
     * @return The Validator instance.
     */
    @Produces
    public Validator createValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }
}
