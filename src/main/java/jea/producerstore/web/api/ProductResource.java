package jea.producerstore.web.api;

import jea.producerstore.app.exception.CreationException;
import jea.producerstore.app.exception.DeletionException;
import jea.producerstore.app.exception.UpdateException;
import jea.producerstore.app.interceptor.LoggedInterceptor;
import jea.producerstore.model.domain.Product;
import jea.producerstore.service.product.ProductService;
import jea.producerstore.web.api.filter.Authorized;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;

import static jea.producerstore.app.util.ResponseUtilities.*;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Interceptors(LoggedInterceptor.class)
public class ProductResource {

    @Inject
    private ProductService productService;

    /**
     * Get a list of all products.
     * @return  HTTP 200 - A list of all products.
     *          HTTP 500 - Something went wrong when retrieving the products.
     */
    @GET
    public Response getAll() {
        try {
            return Response.ok()
                    .entity(productService.getAll())
                    .build();
        } catch (Exception e) {
            return internalServerError(e.getMessage());
        }
    }

    /**
     * Get a specific product using its ID.
     * @param id The ID associated with the product.
     * @param uriInfo Injected property, used to get data about the URI.
     * @return  HTTP 200 - The product is returned to the client.
     *          HTTP 404 - No product with the given ID was found.
     */
    @GET
    @Path("/{id}")
    public Response getById(@PathParam("id") long id, @Context UriInfo uriInfo) {
        try {
            return Response.ok()
                    .entity(productService.getById(id))
                    .link(uriInfo.getAbsolutePathBuilder().build(id), "self")
                    .build();
        } catch (NotFoundException e) {
            return notFound(id, Product.class);
        }
    }

    /**
     * Create a new product.
     * @param product The product to be persisted.
     * @param uriInfo Injected property, used to get data about the URI.
     * @return  HTTP 201 - The product was created successfully, it's ID is returned to the client.
     *          HTTP 500 - Something went wrong during the creation of the product.
     */
    @POST
    @Authorized(roles = {"admin"})
    public Response createProduct(@Valid Product product, @Context UriInfo uriInfo) {
        try {
            long createdId = productService.add(product);

            URI uri = uriInfo.getAbsolutePathBuilder().segment(Long.toString(createdId)).build();

            return Response.created(uri)
                    .link(uriInfo.getAbsolutePathBuilder().build(), "self")
                    .build();
        } catch (CreationException e) {
            return internalServerError(e.getMessage());
        }
    }

    /**
     * Delete a specific product.
     * @param id The ID associated with the product.
     * @return  HTTP 201 - The product was deleted.
     *          HTTP 404 - No product was found with the given ID.
     *          HTTP 500 - Something went wrong during the deletion of the product.
     */
    @DELETE
    @Path("/{id}")
    @Authorized(roles = {"admin"})
    public Response deleteProduct(@PathParam("id") long id) {
        try {
            productService.delete(id);
            return Response.status(204)
                    .build();

        } catch (NotFoundException e) {
            return notFound(id, Product.class);
        } catch (DeletionException e) {
            return internalServerError(e.getMessage());
        }
    }

    /**
     * Update a specific product.
     * @param id The ID associated with the product.
     * @param newValues The values to be updated.
     * @return  HTTP 204 - The product was updated successfully.
     *          HTTP 404 - No product was found with the given ID.
     *          HTTP 500 - Something went wrong during the update.
     */
    @PUT
    @Path("/{id}")
    @Authorized(roles = {"admin"})
    public Response updateProduct(@PathParam("id") long id, Product newValues) {
        try {
            productService.update(id, newValues);
            return Response.status(204)
                    .build();

        } catch (NotFoundException e) {
            return notFound(id, Product.class);
        } catch (UpdateException e) {
            return internalServerError(e.getMessage());
        }
    }
}
