package jea.producerstore.web.api.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import jea.producerstore.app.exception.UnauthorizedException;

import javax.annotation.Priority;
import javax.json.Json;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Authorized
@Priority(Priorities.AUTHENTICATION)
public class AuthorizedFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    private JWTVerifier verifier = JWT.require(Algorithm.HMAC256(System.getProperty("PRODUCERSTORE_JWT_SECRET")))
            .withIssuer(System.getProperty("PRODUCERSTORE_JWT_ISSUER"))
            .build();

    @Override
    public void filter(ContainerRequestContext requestContext) {
        try {
            verifyJwt(requestContext);
        } catch (UnauthorizedException e) {
            requestContext.abortWith(Response.status(401)
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity(e.getMessage())
                    .build());
        }
    }

    private void verifyJwt(ContainerRequestContext context) throws UnauthorizedException {
        Authorized authorized = resourceInfo.getResourceMethod().getAnnotation(Authorized.class);

        String token = getToken(context);

        if (token == null) {
            throw new UnauthorizedException("No authorization token provided.");
        }

        try {
            DecodedJWT jwt = verifier.verify(token);

            if (!anyMatch(Arrays.asList(authorized.roles()), getRoles(jwt))) {
                throw new UnauthorizedException("Worng role.");
            }

        } catch (JWTVerificationException e) {
            throw new UnauthorizedException("Invalid or unauthorized JWT.");
        }
    }

    private List<String> getRoles(DecodedJWT jwt) {
        String payload = new String(Base64.getDecoder().decode(jwt.getPayload()));

        return Json.createReader(new StringReader(payload))
                .readObject()
                .getJsonArray(System.getProperty("PRODUCERSTORE_JWT_ROLE_CLAIM"))
                .stream()
                .map(j -> j.toString().replace("\"", ""))
                .collect(Collectors.toList());
    }

    private String getToken(ContainerRequestContext context)  {
        try {
            String authHeader = context.getHeaders().getFirst("Authorization");

            if (authHeader == null) {
                return null;
            }

            String[] token = authHeader.split("Bearer ");

            if (token.length != 2) {
                return null;
            }

            return token[1];
        } catch (Exception e) {
            return null;
        }
    }

    private boolean anyMatch(List<String> allowedRoles, List<String> providedRoles) {
        for (String allowedRole : allowedRoles) {
            for (String providedRole : providedRoles) {
                if (allowedRole.equals(providedRole)) {
                    return true;
                }
            }
        }

        return false;
    }
}
