package jea.producerstore.web.api;

import jea.producerstore.app.exception.UnauthorizedException;
import jea.producerstore.app.interceptor.LoggedInterceptor;
import jea.producerstore.model.dto.api.Credential;
import jea.producerstore.model.dto.api.OauthResponse;
import jea.producerstore.model.dto.api.JwtToken;
import jea.producerstore.model.dto.api.Oauth;
import jea.producerstore.model.domain.User;
import jea.producerstore.service.authentication.AuthenticationService;
import jea.producerstore.service.authentication.JwtService;

import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import static jea.producerstore.app.util.ResponseUtilities.internalServerError;

@Path("/authenticate")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Interceptors(LoggedInterceptor.class)
public class AuthenticateResource {

    @Inject
    private JwtService jwtService;

    @Inject
    private AuthenticationService authenticationService;

    @Context
    private SecurityContext securityContext;

    /**
     * Authenticate a user with a username and password.
     * @param credential The username and password of the user.
     * @return  HTTP 200 - The user is authenticated and their info is returned.
     *          HTTP 403 - No user was found with the provided username and password
     *          HTTP 500 - Something went wrong during the authentication process.
     */
    @POST
    public Response authenticate(Credential credential) {
        try {
            User user = authenticationService.authenticate(credential);
            JwtToken token = jwtService.createToken(user.getId(), user.getUsername(), user.getRole());

            return Response.ok(token).build();

        } catch (UnauthorizedException e) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();

        } catch (Exception e) {
            return internalServerError(e.getMessage());
        }
    }

    /**
     * Verify an access token provided by Google.
     * @param accessToken The token acquired from Google.
     * @return  HTTP 200 - The token was valid, returns a JWT token (with the 'customer' role) to the client.
     *          HTTP 401 - The token was invalid
     *          HTTP 500 - Something went wrong during the validation of the token.
     */
    @POST
    @Path("/oath/google")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response verifyGoogleOauth(String accessToken) {
        OauthResponse response = authenticationService.verifyOauth(accessToken, Oauth.Google);

        if (!response.isValid()) {
            return Response.status(401).entity("User not authorized").build();
        }

        try {
            JwtToken token = jwtService.createTokenForOauth(response.getUserId(), response.getUsername(), "oauth/v2/google", "customer");

            return Response.ok(token).build();

        } catch (Exception e) {
            return internalServerError(e.getMessage());
        }
    }
}
