package jea.producerstore.web.api;

import jea.producerstore.web.api.filter.AuthorizedFilter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class Api extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(){{
            add(AuthenticateResource.class);
            add(ProductResource.class);
            add(AuthorizedFilter.class);
        }};
    }
}
