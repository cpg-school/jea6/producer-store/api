package jea.producerstore.model.domain;

import jea.producerstore.model.validation.Extension;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;

import javax.validation.constraints.*;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @NotBlank(message = "Name cannot be empty.")
    @Length(max = 200, message = "Name cannot be longer than 200 characters.")
    private String name;

    @Column(columnDefinition = "text")
    @NotBlank(message = "Description cannot be empty.")
    @Size(min = 20, max = 5000, message = "Description can contain 20 to 5000 characters.")
    private String description;

    @Column
    @DecimalMin(value = "0.01", message = "Price must be 0.01 or higher.")
    private double price;

    @Column(columnDefinition = "text")
    @NotBlank(message = "ImageUrl cannot be empty.")
    @URL(regexp = "^(http|https).*", message = "ImageUrl needs to be a valid url.")
    @Extension(message = "ImageUrl needs to be a .jpg, .png or .gif.", allowedExtensions = {"jpg", "png", "gif"})
    private String imageUrl;

    public Product() {

    }

    public Product(String name, String description, double price, String imageUrl) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    //region getters & setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //endregion
}
