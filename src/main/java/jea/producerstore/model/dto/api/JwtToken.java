package jea.producerstore.model.dto.api;

public class JwtToken {

    private String value;

    public JwtToken() {

    }

    public JwtToken(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
