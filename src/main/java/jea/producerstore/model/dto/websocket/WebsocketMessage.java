package jea.producerstore.model.dto.websocket;

public abstract class WebsocketMessage {
    private String type;

    WebsocketMessage(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
