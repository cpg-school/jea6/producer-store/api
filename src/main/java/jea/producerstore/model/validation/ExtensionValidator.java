package jea.producerstore.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Optional;

public class ExtensionValidator implements ConstraintValidator<Extension, String> {
    private String[] allowedExtensions;

    public void initialize(Extension constraint) {
        allowedExtensions = constraint.allowedExtensions();
    }

    public boolean isValid(String url, ConstraintValidatorContext context) {
        String extension = getExtension(url);

        if (extension == null) {
            return false;
        }

        return Arrays
                .asList(allowedExtensions)
                .contains(extension);
    }

    private static String getExtension(String url) {
        return Optional.ofNullable(url)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(url.lastIndexOf(".") + 1))
                .orElse(null);
    }
}
