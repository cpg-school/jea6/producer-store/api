package jea.producerstore.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = ExtensionValidator.class)
@Target( { FIELD })
@Retention(RUNTIME)
public @interface Extension {

    String message() default "Extension is wrong.";
    Class<? extends Payload>[] payload() default {};
    Class<?>[] groups() default {};
    String[] allowedExtensions() default {};
}
