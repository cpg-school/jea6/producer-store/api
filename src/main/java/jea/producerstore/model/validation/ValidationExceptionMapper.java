package jea.producerstore.model.validation;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Iterator;
import java.util.Set;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        Set<ConstraintViolation<?>> validations = exception.getConstraintViolations();

        Class<?> classs = validations.stream().findFirst().get().getRootBeanClass();

        String message = "Incorrectly formatted ".concat(classs.getSimpleName()).concat(":\n");

        Iterator<ConstraintViolation<?>> iterator = validations.iterator();

        while (iterator.hasNext()) {
            ConstraintViolation<?> current = iterator.next();

            message = message.concat("\t")
                    .concat(current.getPropertyPath().toString())
                    .concat(": ")
                    .concat("\"")
                    .concat(current.getMessage())
                    .concat("\"");

            if (iterator.hasNext()) {
                message = message.concat("\n");
            }
        }

        return Response.status(400)
                .entity(message)
                .build();
    }
}
